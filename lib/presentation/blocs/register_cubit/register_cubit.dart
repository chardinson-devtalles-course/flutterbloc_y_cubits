import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutterbloc_y_cubits/infrastructure/inputs/email.dart';
import 'package:flutterbloc_y_cubits/infrastructure/inputs/password.dart';
import 'package:flutterbloc_y_cubits/infrastructure/inputs/user.dart';
import 'package:formz/formz.dart';

part 'register_state.dart';

class RegisterCubit extends Cubit<RegisterFormState> {
  RegisterCubit() : super(const RegisterFormState());

  void onSubmit() {
    emit(
      state.copyWith(
        formStatus: FormStatus.validating,
        user: User.dirty(state.user.value),
        password: Password.dirty(state.password.value),
        email: Email.dirty(state.email.value),
        isValid: Formz.validate([
          state.user,
          state.email,
          state.password,
        ]),
      ),
    );
  }

  void userChanged(String value) {
    final user = User.dirty(value);
    emit(
      state.copyWith(
        user: user,
        isValid: Formz.validate([user, state.email, state.password]),
      ),
    );
  }

  void emailChanged(String value) {
    final email = Email.dirty(value);
    emit(
      state.copyWith(
        email: email,
        isValid: Formz.validate([email, state.user, state.password]),
      ),
    );
  }

  void passwordChanged(String value) {
    final password = Password.dirty(value);
    emit(
      state.copyWith(
        password: password,
        isValid: Formz.validate([password, state.user, state.email]),
      ),
    );
  }
}
