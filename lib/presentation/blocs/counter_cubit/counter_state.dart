part of 'counter_cubit.dart';

class CounterState extends Equatable {
  final int counter;
  final int transactionalCount;

  const CounterState({this.counter = 0, this.transactionalCount = 0});
  copyWith({int? counter, int? transactionalCount}) => CounterState(
      counter: counter ?? this.counter,
      transactionalCount: transactionalCount ?? this.transactionalCount);

  @override
  List<Object?> get props => [counter, transactionalCount];
}
