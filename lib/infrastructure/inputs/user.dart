import 'package:formz/formz.dart';

enum UserInputError { empty, length }

class User extends FormzInput<String, UserInputError> {
  const User.pure() : super.pure('');

  const User.dirty(String value) : super.dirty(value);

  String? get errorMessage {
    if (isValid || isPure) return null;
    if (displayError == UserInputError.empty) return 'Is required';
    if (displayError == UserInputError.length) return 'Min 6 characters';
    return null;
  }

  @override
  UserInputError? validator(String value) {
    if (value.isEmpty || value.trim().isEmpty) return UserInputError.empty;
    if (value.length < 6) return UserInputError.length;
    return null;
  }
}
